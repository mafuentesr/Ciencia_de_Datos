# Ciencia_de_Datos

 Este repositorio contiene los desarrollos propuestos para los retos y talleres pertenecientes a la asignatura ciencia de datos.
 
 El primer reto consiste en la elaboración de una pagina web capaz de conectarce con la api de Datos abiertos de colombia, esta aplicacion permite observar un conjunto de datos sobre vacunación contra el covid19 actulizado en tiempo real, permite realizar busquedas avanzadas y realizar analisis de la información, las tecnologias utilizadas para la elaboración de este proyecto fueron : Python,DJango,JavaScript,Html5,Css,Ajax. 
 
 El segundo reto consiste en un realizar Procesamiento de lenguajes naturales de los comentarios de la red social Twitter, esto para hacer un analisis de las emociones de los comentarios de los usuarios. 
 
 
 El reto final consiste en la implementación de un sistema de reconocimiento en tiempo real de 43 señales de trafico utilizando redes neuronales convolucionales, los lenguajes Utilizados para la elboración de este proyecto fueron : Javascript,Python . 
 
 Repositorio Elaborado por :  https://www.linkedin.com/in/miguelafuentesr/ 
